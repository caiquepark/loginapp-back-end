var express = require('express'); 
var mongoose = require('mongoose');
var cors = require('cors');
var api = require('./routes/api');
var auth = require('./routes/auth');
var app = express();


app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors());

mongoose.connect('mongodb://localhost:27017/auteticacao_teste', {
   useUnifiedTopology: true,
   useNewUrlParser: true
});

app.use('/api', api);
app.use('/auth', auth);

// rotas://

app.use(function(req, res, next){
    res.status(404).send("Rota não encontrada");
})

app.listen(3000);