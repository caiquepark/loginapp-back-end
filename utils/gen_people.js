var mongoose = require('mongoose');
var faker = require('faker');
var PersonModel = require('../models/PersonModel');


mongoose.connect('mongodb://localhost:27017/auteticacao_teste', {
   useUnifiedTopology: true,
   useNewUrlParser: true
});
 
async function  add(n) {
    try {
        for (let i = 0; i<n; i++) {
            const P = new PersonModel();
            P.name = faker.name.firstName();
            P.country = faker.address.country();
            P.email = faker.internet.email();
            P.company = faker.company.companyName();
            await P.save();
            
        }
        
    } catch (error) {
        console.log(error);

    }

}

add(100)
    .then(()=> {
        console.log("ok");
        mongoose.disconnect();
    })