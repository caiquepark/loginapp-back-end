var mongoose = require('mongoose');
var faker = require('faker');
var ProductModel = require('../models/ProductModel');


mongoose.connect('mongodb://localhost:27017/auteticacao_teste', {
   useUnifiedTopology: true,
   useNewUrlParser: true
});
 
async function  add(n) {
    try {
        for (let i = 0; i<n; i++) {
            const P = new ProductModel();
            P.name = faker.commerce.productName();
            P.department = faker.commerce.department();
            P.price = faker.commerce.price(); 
            await P.save();
            
        }
        
    } catch (error) {
        console.log(error);

    }

}

add(100)
    .then(()=> {
        console.log("ok");
        mongoose.disconnect();
    })