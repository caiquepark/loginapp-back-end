var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    firstName: String,
    lastName: String,
    address: String,
    city: String,
    state: String,
    phone: Number,
    mobilePhone: Number,
    email: String,
    password: String
})

module.exports = mongoose.model('User', userSchema)