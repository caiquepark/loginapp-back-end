var express = require('express');
var router = express.Router();
const Auth_controller = require('../controllers/Auth_contoller');


router.post('/login', Auth_controller.login);
router.post('/register', Auth_controller.register);

router.use(Auth_controller.check_token);
router.get('/user', Auth_controller.user_data);

module.exports = router;