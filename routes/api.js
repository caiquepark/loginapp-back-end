var express = require('express');
var router = express.Router();
var PersonController = require('../controllers/Person_controller');
var ProductController = require('../controllers/Product_controller');
var AuthController = require('../controllers/Auth_contoller');


router.use(AuthController.check_token);

router.get('/people', PersonController.all);
router.get('/products', ProductController.all);

module.exports = router;